
<div class="wrap block">
	<h1><?php echo $GLOBALS['title']; ?></h1>
	<div class="block">
		<!-- Get page title -->
				<input id="page_title_field" type="text" name="PageTitle" placeholder="Page title" />
				<h2>Select a component you want to add</h2>
				<div id="componentsMainDiv">
					
				</div>
				
				<input class="submitButton" id="add_component_bt" type="button" name="add_components" value="Add components" />
				<!-- Add components button -->

				<div id="component_list">

				</div>

				<input class="submitButton" id="save_page" type="button" name="save_page" value="Create Page" />
	</div>

</div>



<script>

var componentsData = [];
var hCollectionItems = [];
function isURL(str) {
  var a  = document.createElement('a');
   a.href = str;
   return (a.host && a.host != window.location.host);
}

function addComponent(type, value)
{
	var obj = [];
	obj["type"] = type;
	obj["value"] = value;
	componentsData.push(obj);
	console.log('adding component for type ' + type + ' with value ' + value);

	addComponentPreview(obj);
}

function previewImageComponentStyle(component)
{
	var style = {
		"flex":1,
		"object-fit":"contain",
		"width":"100%"
	}
	return style;
}

function previewTextComponentStyle(component)
{
	var style = {
		"font-weight": "bold",
		"font-size": 20,
		"text-align": "center"
	}
	return style;
}

function applyStyleOnDOMElement(style, element)
{
	 for (var key in style) {
		 	try {
		 		if (style.hasOwnProperty(key)) {     
			 		element.style[key] = style[key];
			 	}else {
			 		console.log('unable to apply style on component for key ' + key);
			 	}	
	 	}catch (e){
	 		console.log("exception, " + e);
	 	}
	 	
	 }
}

function addImageComponent(component, containerDiv)
{
	console.log('will add image component preview');
	const src = component["value"];
	var img_element = document.createElement("img");
	img_element.src = src;

	containerDiv.appendChild(img_element);

	applyStyleOnDOMElement(previewImageComponentStyle(component), img_element);

}

function addTextComponent(component, containerDiv)
{
	// Best tiger ever
	console.log('will add text component preview');
	const text = component["value"];
	var p_element = document.createElement("p");
	p_element.innerHTML = text;

	containerDiv.appendChild(p_element);

	applyStyleOnDOMElement(previewTextComponentStyle(component), p_element);	
}

function addHorizontalComponent(component, containerDiv)
{

}

function addComponentPreview(component){

	console.log('addComponentPreview');

	var preview_row = document.createElement("div");
	preview_row.setAttribute("class", "preview_rowdiv");

	var preview_design = document.createElement("div");
	preview_design.setAttribute("class", "preview_designdiv");

	var preview_design_prop = document.createElement("div");
	preview_design_prop.setAttribute("class", "preview_designpropdiv");

	preview_row.appendChild(preview_design);
	preview_row.appendChild(preview_design_prop);
	
	var div_component_list = document.getElementById("component_list");

	applyStyleOnDOMElement();
	

	switch (component["type"])
	{
		case "image":
		{
			div_component_list.appendChild(preview_row);
			addImageComponent(component, preview_design);
			break;
		}

		case "text":
		{
			div_component_list.appendChild(preview_row);
			addTextComponent(component, preview_design);
			break;
		}

		case "horizontal_collection":
		{
			addHorizontalComponent(component, preview_design);
			break;
		}

	}
}

function validateAddHComponentItems()
{
	const input_collection_item_type = document.getElementById("hCollectionItemContentType");
			switch (input_collection_item_type.value)
			{
				case "image":
				{
					const field = document.getElementById("h_collection_input_imgsrc");
					const j={"type":input_collection_item_type.value, "value":field.value};
					const value = jsonJSON.stringify(j);
					hCollectionItems.push(value);
				}
				break;

				case "algolia-product":
				{
					const field = document.getElementById("h_collection_input_algoliaprod");
					const j={"type":input_collection_item_type.value, "value":field.value};
					const value = jsonJSON.stringify(j);
					hCollectionItems.push(value);
				}
				break;

				case "algolia-search":
				{
					const textarea = document.getElementById("h_collection_input_textarea");
					try {
   						const valJSON = JSON.parse(textarea.value);
						const j={"type":input_collection_item_type.value, "value":valJSON};
						const value = JSON.stringify(j);
						hCollectionItems.push(value);
						textarea.value = "";
						console.log("item pushed in hcollection");
					}catch (e) {
					   // statements to handle any exceptions
					   console.log("exception caught: " + e );
					}
					
				}
				break;

				default:{
					break;
				}
			}
}

function prepareJSONFromHCollections()
{
	try {
		const j = JSON.stringify(hCollectionItems);
		return j;
	} catch (e)
	{
		console.log('exception, ' + j);
	}

}

function validateAddComponentFields()
{
	const select_component_type_dom = document.getElementById("component_select_type");
	const select_component_type_value = select_component_type_dom.value;
	switch (select_component_type_value)
	{
		case "image":
		{
			console.log("img");
			const input = document.getElementById("input_image_url_add_comp");
			if (isURL(input.value))
			{
				console.log("valid component");
				addComponent(select_component_type_value, input.value);
			} else {
				console.log("invalid component data");
			}
		}
		break;
		case "text":
		{
			console.log("text");
			const input = document.getElementById("input_text_url_add_comp");
			if (input.value.length > 0)
			{
				console.log("valid component");
				addComponent(select_component_type_value, input.value);
			} else {
				console.log("invalid component data");
			}

		}
		break;
		case "horizontal_collection":
		{
			console.log("h_collection");
			if (hCollectionItems.length > 0)
			{
				addComponent(select_component_type_value, prepareJSONFromHCollections());
				hCollectionItems = [];
			}else {
				console.log("no items in collection");
			}
		}
		break;

		default: {

			console.log("unkown type of component: " + select_component_type_value);
		}
	}
	return false;
}

function clearChildrens(node)
{
	if (node == null || node == undefined )
	{
		return;
	}
    while (node.children.length > 0) {
        node.removeChild(node.lastChild);
    }
}

//name="Text1" cols="40" rows="5"
function setComponentTextAreaField(parent, placeholder = null)
{
	var input = document.createElement("textarea");
	input.name = "algoliaJSONField";
	input.id = "algolia_json_field";
	input.placeholder = placeholder;
	parent.appendChild(input);
	return input;
}

function setComponentInputField(parent, placeholder)
{
	var input = document.createElement("input");
	input.setAttribute("class", "textfield");
	input.setAttribute('type', 'text');
	input.placeholder = placeholder;
	parent.appendChild(input);
	return input;
}

function selecDOMElement(data) {
	 var selectElement = document.createElement("select");
    selectElement.setAttribute("class", "selector")

    for (var key in data) {
	    // check if the property/key is defined in the object itself, not in parent
	    if (data.hasOwnProperty(key)) {           
	        console.log(data[key]);
	        var optionElement = document.createElement("option");
   		 	optionElement.label = data[key].value;
   		 	optionElement.value = data[key].key;
   		 	selectElement.appendChild(optionElement);
	    }
	}

	return selectElement;
}

function setNewHCollectionDivChild(type) {
		console.log('value changed ' + type);
		var form = document.getElementById("new_h_collection_div");
		if (form == null){
			return;
		}
		clearChildrens(form);

		
		if (type == "image"){
			const field = setComponentInputField(form, "Image url");
			field.id = "h_collection_input_imgsrc";
		} else if (type == "algolia-product") {
			const field = setComponentInputField(form, "Algolia Product ID");
			field.id = "h_collection_input_algoliaprod";
		} else if (type == "algolia-search") {
			const tArea = setComponentTextAreaField(form, "Algolia JSON");
			tArea.id = "h_collection_input_textarea";
		}

}

function addSelectHCollectionElement(parent)
{
	const data = [{key:"image", value:"Image"}, {key: "algolia-product", value: "Product"}, {key: "algolia-search", value: "Algolia Search Filter"}];
    
    var selectElement = selecDOMElement(data);
    selectElement.id = "hCollectionItemContentType"
	selectElement.onchange = function(){
		hCollectionItems = [];
		var strUser = selectElement.options[selectElement.selectedIndex].value;
		setNewHCollectionDivChild(strUser);

	};
	parent.appendChild(selectElement);

}

function setComponentHorizontalCollection(parent)
{
	var div = document.createElement("div");
	var addButton = document.createElement("input");
	addButton.setAttribute('type', 'button');
	addButton.value = "Add item";
	addButton.id = "add__collection_item_bt"
	div.id = "new_h_collection_div";
	div.setAttribute('class', 'block');
	addSelectHCollectionElement(parent);
	parent.appendChild(div);
	parent.appendChild(addButton);

	setNewHCollectionDivChild("image");

	addButton.onclick = function(){
		validateAddHComponentItems();
	}
}


function setComponentForm(type, parent) {
	// body...
	clearChildrens(parent);
	if (type === "image")
	{
		const input = setComponentInputField(parent, "image url");
		input.id = "input_image_url_add_comp";
	} else if (type === "text")
	{
		const input = setComponentInputField(parent, "Text");
		input.id = "input_text_url_add_comp";
	}else if (type === "horizontal_collection")
	{
		setComponentHorizontalCollection(parent);
	} else {
		parent.innerHTML = "<p>todo</p>";
	}
}

function selectComponentType()
{
	const compTypes = [{key:"image", value:"Image"}, {key: "text", value: "Text"}, {key:"horizontal_collection", value: "Horizontal Collection"}];
    
    var selectElement = selecDOMElement(compTypes);
    selectElement.name = "component_types";


	selectElement.onchange = function(){
		var strUser = selectElement.options[selectElement.selectedIndex].value;
		console.log('value changed ' + strUser);
		var parent = selectElement.parentNode;
		var form = document.getElementById("component_input_data_div");
		setComponentForm(strUser, form);
	};


	return selectElement;
}

function addComponents() {
    var mainDiv = document.getElementById("componentsMainDiv");
    const index = mainDiv.childElementCount;
	// component div
    var div = document.createElement("div");
    div.setAttribute('class', 'block componentDiv');
	div.setAttribute('id', 'add_component_div');

    // component type 
    var component_input_heading = document.createElement("h4");
    component_input_heading.innerHTML = "Select a data type of component";
    var selectElement = selectComponentType();
    selectElement.id = "component_select_type";
	//div.appendChild(component_input_heading);
	div.appendChild(selectElement);

    var formDiv = document.createElement("div");
    formDiv.id = "component_input_data_div"
    formDiv.setAttribute('class', 'block');
    div.appendChild(formDiv);

    setComponentForm("image", formDiv);
	mainDiv.appendChild(div);
}

function setAddComponentBt()
{
	const bt = document.getElementById("add_component_bt");
	bt.onclick = function(){
		validateAddComponentFields();
	}
}

addComponents();
setAddComponentBt();

</script>