<?php
/*
Plugin Name:  SDG React Native App Designer
Plugin URI:   https://facebook.com/FantasticAtif
Description:  Create App Screens Dynamically in plugins with one signal integration, api for app and different design for different screen
Version:      20171123
Author:       Atif Khan
Author URI:   https://facebook.com/FantasticAtif
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  wporg
Domain Path:  /languages
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' ); 
add_action( 'init', 'reactnative_register_plugin_styles' );
/**
 * Register style sheet.
 */
    
function reactnative_register_plugin_styles() {
    $date = date_create();
    wp_register_style( 'sdg-reactnative-app-designer', plugins_url( 'sdg-reactnative-app-designer/admin/main.css?' . date_timestamp_get($date) ) );
    wp_enqueue_style( 'sdg-reactnative-app-designer' );
}
?>

<?php 
//https://codex.wordpress.org/Writing_a_Plugin
//https://developer.wordpress.org/plugins/the-basics/#getting-started

function pluginInit() {
	global $wpdb;

   	$table_name = $wpdb->prefix . "reactnative_app_design_page";     

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name(
    id MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
    TIME DATETIME DEFAULT NOW() NOT NULL,
    title TINYTEXT NOT NULL,
    pageType ENUM('home') DEFAULT 'home' NOT NULL,
    version TINYTEXT NOT NULL,
    props LONGTEXT,
    PRIMARY KEY(id)
) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	$components_table_name = $wpdb->prefix . "reactnative_app_design_components";     

	$sql = "CREATE TABLE $components_table_name(
    id MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
    pageId MEDIUMINT(9) NOT NULL,
    componentKey TINYTEXT NOT NULL,
    componentType ENUM(
        'image',
        'text',
        'horizontal-collection',
        'vertical-collection'
    ) NOT NULL,
    props LONGTEXT,
    PRIMARY KEY(id),
    FOREIGN KEY (pageId) REFERENCES $table_name(id)
) $charset_collate;";

	dbDelta( $sql );

}

function sidemenu_settings()
{
    // check user capabilities
    if (!current_user_can('manage_options')) {
        return;
    }
    include plugin_dir_path( __FILE__ ) . 'admin/settings.php';
}

function sidemenu_new()
{
    // check user capabilities
    if (!current_user_can('manage_options')) {
        return;
    }
    include plugin_dir_path( __FILE__ ) . 'admin/new.php';
}

function sidemenu_view_all()
{
    // check user capabilities
    if (!current_user_can('manage_options')) {
        return;
    }
    include plugin_dir_path( __FILE__ ) . 'admin/view_all.php';
}

function wporg_options_page()
{
    add_menu_page(
        'View All Pages',
        'React Native',
        'manage_options',
        'react_native_all',
       	'sidemenu_view_all',
        plugin_dir_url(__FILE__) . 'images/icon_wporg.png',
        null
    );
    add_submenu_page(
  	'react_native_all',
    'View All Pages',
    'All Pages',
    'manage_options',
    'react_native_all',
    'sidemenu_view_all'
	);

	add_submenu_page(
  	'react_native_all',
    'New Pages',
    'New Pages',
    'manage_options',
    'react_native_new',
    'sidemenu_new'
	);

	add_submenu_page(
  	'react_native_all',
    'Settings',
    'Settings',
    'manage_options',
    'settings',
    'sidemenu_settings'
	);
}
add_action('admin_menu', 'wporg_options_page');

add_action( 'activate_sdg-reactnative-app-designer/sdg-reactnative-app-designer.php', 'pluginInit' );

?>


